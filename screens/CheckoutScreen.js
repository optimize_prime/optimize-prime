/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React, { Component } from 'react';
import { StyleSheet, TextInput, View, Image, Text, TouchableOpacity, StatusBar, KeyboardAvoidingView } from 'react-native';
import firebase from 'firebase';
import RNPickerSelect from 'react-native-picker-select';
import { ScrollView } from 'react-native-gesture-handler';
import Fire from '.././Firebase/Fire.js';

// list of states to create the location-picker. (50 states), only the United States
/* Author: Dante DeSantis
 * This screen will appear after the user decides to purchase something from the app.
 * The user will have to input their billing and shipping information.
 * The database will only store the shipping information given by the user.
 * This class contains functions that communicate with the Database.
**/

// List of countries to pass to the RNPickerSelector to have values the user can pick from.

const states = [
    {
        label: 'Alabama',
        value: 'Alabama'
    },
    {
        label: 'Alaska',
        value: 'Alaska'
    },
    {
        label: 'Arizona',
        value: 'Arizona'
    },
    {
        label: 'Arkansas',
        value: 'Arkansas'
    },
    {
        label: 'California',
        value: 'California'
    },
    {
        label: 'Colorado',
        value: 'Colorado'
    },
    {
        label: 'Connecticut',
        value: 'Connecticut'
    },
    {
        label: 'Delaware',
        value: 'Delaware'
    },
    {
        label: 'Florida',
        value: 'Florida'
    },
    {
        label: 'Georgia',
        value: 'Georgia'
    },
    {
        label: 'Hawaii',
        value: 'Hawaii'
    },
    {
        label: 'Idaho',
        value: 'Idaho'
    },
    {
        label: 'Illinois',
        value: 'Illinois'
    },
    {
        label: 'Indiana',
        value: 'Indiana'
    },
    {
        label: 'Iowa',
        value: 'Iowa'
    },
    {
        label: 'Kansas',
        value: 'Kansas'
    },
    {
        label: 'Kentucky',
        value: 'Kentucky'
    },
    {
        label: 'Louisiana',
        value: 'Louisiana'
    },
    {
        label: 'Maine',
        value: 'Maine'
    },
    {
        label: 'Maryland',
        value: 'Maryland'
    },
    {
        label: 'Massachusetts',
        value: 'Massachusetts'
    },
    {
        label: 'Michigan',
        value: 'Michigan'
    },
    {
        label: 'Minnesota',
        value: 'Minnesota'
    },
    {
        label: 'Mississippi',
        value: 'Mississippi'
    },
    {
        label: 'Missouri',
        value: 'Missouri'
    },
    {
        label: 'Montana',
        value: 'Montana'
    },
    {
        label: 'Nebraska',
        value: 'Nebraska'
    },
    {
        label: 'Nevada',
        value: 'Nevada'
    },
    {
        label: 'New Hampsire',
        value: 'New Hampsire'
    },
    {
        label: 'New Jersey',
        value: 'New Jersey'
    },
    {
        label: 'New Mexico',
        value: 'New Mexico'
    },
    {
        label: 'New York',
        value: 'New York'
    },
    {
        label: 'North Carolina',
        value: 'North Carolina'
    },
    {
        label: 'North Dakota',
        value: 'North Dakota'
    },
    {
        label: 'Ohio',
        value: 'Ohio'
    },
    {
        label: 'Oklahoma',
        value: 'Oklahoma'
    },
    {
        label: 'Oregon',
        value: 'Oregon'
    },
    {
        label: 'Pennsylvania',
        value: 'Pennsylvania'
    },
    {
        label: 'Rhode Island',
        value: 'Rhode Island'
    },
    {
        label: 'South Carolina',
        value: 'South Carolina'
    },
    {
        label: 'South Dakota',
        value: 'South Dakota'
    },
    {
        label: 'Tennessee',
        value: 'Tennessee'
    },
    {
        label: 'Texas',
        value: 'Texas'
    },
    {
        label: 'Utah',
        value: 'Utah'
    },
    {
        label: 'Vermont',
        value: 'Vermont'
    },
    {
        label: 'Virginia',
        value: 'Virginia'
    },
    {
        label: 'Washington',
        value: 'Washington'
    },
    {
        label: 'West Virginia',
        value: 'West Virginia'
    },
    {
        label: 'Wisconsin',
        value: 'Wisconsin'
    },
    {
        label: 'Wyoming',
        value: 'Wyoming'
    },



];
export default class Checkout extends Component {
    // ship state will have varaibles to store information that the user provides.

    state = {
        ship: {
            city: '',
            streetAdd: '',
            zip: '',
            state: ''
        }

    };





    // handleCheckout => This method will call another method called checkout() which is located in
    // Fire.js. After the checkout method is complete the app will navigate the user to the Settings screen.

    /* This function will be called when the User taps the Checkout button.
     * The database will be updated with the given shipping information then navigate to
     * ThankyouScreen.
    **/
    handleCheckout = async () => {
        await Fire.shared.checkout(this.state.ship);
        this.props.navigation.navigate('ThankyouScreen');
    };

    render() {
        const placeholder = {  // placeholder for the location-picker.
            label: 'Select a State',
            value: null,
            color: '#9EA0A4',
        };



        return (
            <View style={styles.scrollPageWrap}>
                <ScrollView style={styles.scrollPage}>
                    <KeyboardAvoidingView behavior="padding" style={styles.otherContainer}>
                        <View style={styles.logoContainer}>
                            <Image
                                style={styles.logo}
                                source={require('./resources/logo.png')}
                            />
                        </View>
                        <View style={styles.container}>
                            <StatusBar
                                barStyle="dark-content"
                            />
                            <Text style={styles.title}>Checkout</Text>
                            <TextInput      // Card Name will not be stored
                                placeholder="* Name on Card"
                                returnKeyType="next"
                                onSubmitEditing={() => this.cardNumber.focus()}
                                autoCapitalize="true"
                                autoCorrect={false}
                                style={styles.input}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <TextInput   // Card Number will not be stored
                                placeholder="* Card Number"
                                returnKeyType="next"
                                autoCapitalize="true"
                                autoCorrect={false}
                                style={styles.input}
                                ref={(input) => this.cardNumber = input}
                                onSubmitEditing={() => this.expiration.focus()}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <TextInput    // Card expiration date will not be stored
                                placeholder="* Expiration"
                                returnKeyType="next"
                                autoCapitalize="true"
                                autoCorrect={false}
                                style={styles.input}
                                ref={(input) => this.expiration = input}
                                onSubmitEditing={() => this.cvv.focus()}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <TextInput
                                placeholder="* CCV"
                                returnKeyType="next"
                                keyboardType="email-address"
                                autoCapitalize="none"
                                autoCorrect={false}
                                style={styles.input}
                                ref={(input) => this.cvv = input}
                                onSubmitEditing={() => this.streetAddress.focus()}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <Text style={styles.subTitle}>Shipping Information</Text>
                            <TextInput
                                placeholder="* Street Address"
                                returnKeyType="next"
                                autoCapitalize="true"
                                autoCorrect={false}
                                style={styles.input}
                                ref={(input) => this.streetAddress = input}
                                onSubmitEditing={() => this.city.focus()}
                                // upon text change ship.streetAdd will be updated
                                onChangeText={streetAdd => this.setState({ ship: { ...this.state.streetAdd, streetAdd } })}
                                value={this.state.ship.streetAdd}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <TextInput
                                placeholder="* City"
                                returnKeyType="next"
                                autoCapitalize="true"
                                autoCorrect={false}
                                style={styles.input}
                                ref={(input) => this.zip = input}
                                onSubmitEditing={() => this.city.focus()}
                                // upon text change ship.city will be updated
                                onChangeText={city => this.setState({ ship: { ...this.state.ship, city } })}
                                value={this.state.ship.city}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <TextInput
                                placeholder="* Zip"
                                returnKeyType="next"
                                autoCapitalize="true"
                                autoCorrect={false}
                                style={styles.input}
                                ref={(input) => this.zip = input}
                                // upon text change ship.zip will be updated
                                onChangeText={zip => this.setState({ ship: { ...this.state.ship, zip } })}
                                value={this.state.ship.zip}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <RNPickerSelect  // location-picker initialization
                                placeholder={placeholder}
                                items={country}
                                onValueChange={state => this.setState({ ship: { ...this.state.ship, state } })}
                                onUpArrow={() => {
                                    this.zipInput.focus();
                                }}
                                onDownArrow={() => {
                                }}
                                style={pickerSelectStyles}
                                value={this.state.ship.state}
                            />
                            <TouchableOpacity onPress={this.handleCheckout} style={styles.buttonContainer}>
                                <Text style={styles.buttonText}>Checkout</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>


        );
    }
}

const styles = StyleSheet.create({

    container: {
        marginTop: 50,
        paddingHorizontal: 5,
    },

    input: {
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, 0)',
        paddingHorizontal: 2
    },


    line: {
        height: 10,
        width: 266,
        marginTop: -14,
        color: 'black',
        marginBottom: 10
    },

    buttonContainer: {
        paddingVertical: 12,
        marginBottom: 20,
        marginTop: 15,
        borderRadius: 7,
        borderWidth: 0,
        backgroundColor: '#800000'
    },

    buttonText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 20
    },

    textContainer: {
        marginTop: 5,
        marginBottom: -13,
    },

    title: {
        color: '#800000',
        textAlign: 'center',
        marginTop: -25,
        fontSize: 25,
        fontWeight: '400',
        marginBottom: 15
    },

    subTitle: {
        color: '#800000',
        fontSize: 20,
        fontWeight: '400',
        marginTop: 35,
        marginBottom: 15
    },


    otherContainer: {
        backgroundColor: '#fff',
        paddingHorizontal: 50
    },

    logoContainer: {
        marginTop: 15,
        flexGrow: 1,
    },

    logo: {
        width: 100,
        height: 100,
        marginLeft: 90,
        resizeMode: "contain",
    },

    error: {
        color: '#E9446A',
        fontSize: 13,
        fontWeight: '600',
        textAlign: 'center'
    },

    errorMessage: {
        height: 72,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 30,
    },
    scrollPageWrap: {
        flex: 7,
        flexDirection: 'column',
    },
    scrollPage: {
        flex: 1,
        flexDirection: 'column',
    },


});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 14,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#800000',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30,
    },

    inputAndroid: {
        fontSize: 14,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: '#800000',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30,
    },
});
