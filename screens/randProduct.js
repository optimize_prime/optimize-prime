/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, ScrollView, Image, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Rating } from 'react-native-ratings';
import Firebase from './firebase.js';



export default class RandProduct extends React.Component {

	//state object
  	state = { // Default values
  		isLoaded: false, 
  		wineName: "Error",
  		wineID: 0,
  		imgURL: "{require('../assets/ftProducts/wine-bottle-default.png')}",
  		price: 0.00,
  		rating: 0,
  		winery: "N/A",
  		vintage: "0000",
  		grape: "N/A",
  		origin: "N/A",
  		};


	randID = Math.ceil(Math.random() * (10000 - 1) + 1);	// Generate random number 1-10,000

	componentDidMount() {
			// Get random product data from the DB
			let randProd = Firebase.firestore().collection('Products').doc( this.randID.toString() );
			let getDoc = randProd.get()
			  .then(doc => {
			    if (!doc.exists) {
			      console.log('No such document!');
			    } else { // Load in product data
		    	  this.setState({wineName:doc.data().name});
		    	  this.setState({wineID:doc.id});
			      this.setState({imgURL:doc.data().imageURL});
			      this.setState({price:doc.data().price});
			      this.setState({rating:doc.data().ratingVal});
			      this.setState({winery:doc.data().winery});
			      this.setState({vintage:doc.data().vintage});
			      this.setState({grape:doc.data().grape});
			      this.setState({origin:doc.data().origin});

			      this.setState({isLoaded:true});
			    }
			  })
			  .catch(err => {
			    console.log('Error getting document', err);
			  });
	}

	render() {
		if (!this.state.isLoaded)
			return null;
		// Return the product data as a clickable element
		return(


			<TouchableOpacity onPress = {() => this.props.navigation.navigate('ProductScreen', {
				id: this.state.wineID, 
				name: this.state.wineName, 
				imgURL: this.state.imgURL,
				price: this.state.price,
				rating: this.state.rating,
				winery: this.state.winery,
				vintage: this.state.vintage,
				grape: this.state.grape,
				origin: this.state.origin,
			})} style={styles.productContainer}>
				<View style={styles.prodImgCont}>
					<Image source={{uri: this.state.imgURL}} style={{width: 110, height: 150, resizeMode: "contain"}}  />
				</View>

				<View style={styles.prodInfoCont}>
					<View style={styles.prodInfoRow}>
							<Text style={{fontSize: 15}}>{this.state.wineName}</Text>
					</View>


					<View style={styles.prodInfoRow}>
						<Rating
							type="star"
							startingValue={this.state.rating}
							imageSize={20}
							readonly={true}
							style={styles.prodRating}
						/>
					</View>


					<View style={styles.prodInfoBotRow}>
						<View style={styles.prodInfoColRow}>
						</View>

						<View style={styles.prodInfoColRow}>
							<Text style={{fontSize: 30}}>${this.state.price}</Text>
						</View>
					</View>
				</View>
			</TouchableOpacity>


		);
	} 
}

const styles = StyleSheet.create({

  productContainer: {
  	flex: 1,
  	flexDirection: 'row',
  	borderWidth: 1.5,
  	borderColor: '#81151e',
  	margin: 10,
  },
  prodImgCont: {
  	flex: 1,
  	alignItems: 'center',
  	justifyContent: 'center',
  	borderRightWidth: 1.5,
  	borderColor: '#81151e',
  },
  prodInfoCont: {
	flex: 2,
	flexDirection: 'column',
  },
  prodInfoCol: {
  	flex: 1,
  	flexDirection: 'column'
  },
  prodInfoRow:
  {
  	flex: 1,
  },
  prodInfoBotRow: {
  	flex: 1,
  	flexDirection: 'row'
  },
  prodInfoColRow: {
  	flex: 1,
  	alignItems: 'flex-end',
  	justifyContent: 'flex-end',
  	paddingRight: 5,
  },
  prodRating: {
  	flex: 1,
  	width: 100,
  }
});

