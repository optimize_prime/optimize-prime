/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/

import React from 'react';
import FB from "../screens/firebase.js";
import Fire from '../Firebase/Fire.js';
import {AsyncStorage} from 'react-native';

/* Author: Max Kiniklis
 * This class contains all of the functions for adding products to a cart.
**/

export default class AddToCart {
	
	// Return the currently logged in user from Firebase
	static getUID() {
        return Fire.shared.uid;
	}

	// Create a timestamp to represent when the user adds the product to their cart
	// Timestamp uses the UNIX timestamp. It is written as the # of miliseconds since Jan. 1, 1970
	static getTimestamp() {
		return Date.now();
	}

	// Add a new cart entry into firebase
	static addToFirebase( userID, prodID, name, price, timestamp ) {
        let db = Fire.shared.firestore.collection('UserProfile').doc(Fire.shared.uid).collection('Cart').doc(Fire.shared.uid).collection('Products').doc(prodID);

        db.set({
		  UserID: userID,
            ProductID: prodID,
            ProductName: name,
          ProductPrice: price,
		  Timestamp: timestamp,
		}).then(ref => {
		  console.log('Added ', ref.id, 'to cart in firestore.');
		});
	}

	// Save all of the data locally using AsyncStorage so we can save which items are in our carts
	static async saveLocally( userID, prodID ) {
		// JSON object used for creating new AsyncStorage entry
		let cartObj = {
			userID: userID,
			numItems: 1,
			products: [ {pid: prodID, qty: 1} ],
		};

		// Get Cart data in AsyncStorage and add new product if it does not already exist
		try {
			await AsyncStorage.getItem('Cart', (err, result) => {
				if (result !== null) {		// Cart Exists
					var cart = JSON.parse(result);		// Convert to JSON object
/********************************
*********************************
	FINISH ASYNC USER CONFIRMATION
********************************/
					// Make sure the current user = the user for the shopping cart
					if( cart.userID != userID )
						console.log("Error - Incorrect User Shopping Cart");

					var prodFound = false;
					
					for ( i in cart.products )
					{
						// If the PID of item being added is already in the cart, 
						// increment its quantity by 1 and the total number of items by 1
						if( cart.products[i].pid == prodID )
						{
							cart.products[i].qty += 1;
							cart.numItems += 1;
							prodFound = true;
							console.log("Product already in cart - Quantity updated.\n\n");
						}
					}

					// If the product is not found already in the array, add it. Increment NumItems
					if( !prodFound )
					{
						cart.products.push({"pid":prodID, "qty":1}) 	// Add product to array
						cart.numItems += 1;
						console.log("Product not found in cart. - Product '" + prodID + "'' added to cart.\n\n");
					}

					// Save changes by setting 'Cart' with the new values
					AsyncStorage.setItem('Cart', JSON.stringify(cart));

				} else {	// Cart does not exist
				    console.log("Creating Cart for user and adding products.");
				    AsyncStorage.setItem('Cart', JSON.stringify(cartObj));
				}
			});
		} catch (error) {
			console.log("Error - Something has gone wrong.");
		}
	}

	// Add an item to the cart and call all of the above functions
	static addItem( prodID,name,price ) {
        var PID = prodID;
        var NAME = name;
        var PRICE = price;
		var UID = this.getUID();	// Get the User ID from firebase
		var timestamp = this.getTimestamp();	// Get a current timestamp

		this.addToFirebase( UID, PID, NAME, PRICE, timestamp);	// Add to Firebase
		this.saveLocally( UID, PID );	// Save product to Async Storage
	}
}