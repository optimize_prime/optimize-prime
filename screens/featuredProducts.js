/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, ScrollView, Image, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Rating } from 'react-native-ratings';
import NavBar from './navBar';
import RandProduct from './randProduct.js';


export default class FeatProduct extends React.Component {
	constructor( ) {
			console.disableYellowBox = true;
			super();
			this.state = {
				isLoading: false,
			};
	}

	render() {
		const nav = new NavBar(4, "featProduct", "Login", this.props);

		return(
			<View style={styles.container}>
				{nav.render()}
				<View style={styles.featProdCtnr}>
					<Text style={styles.ftProdHeader}>Featured Product</Text>

					<ScrollView style={styles.ftProdFlatList}>

						{/* RandProduct is a custom element that will randomly select a product from the database */}
						<RandProduct navigation={this.props.navigation}/>
						<RandProduct navigation={this.props.navigation}/>
						<RandProduct navigation={this.props.navigation}/>
						<RandProduct navigation={this.props.navigation}/>
						<RandProduct navigation={this.props.navigation}/>

					</ScrollView>					
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  featProdCtnr: {
  	flex: 7,
  	marginTop: 10,
  },
  ftProdHeader: {
  	color: '#81151e',
  	fontSize: 15,
  	fontWeight: '500',
  	marginLeft: 10,
  },
  ftProdFlatList: {
	flex: 1,
	flexDirection: 'column',
	marginBottom: 20,
  },
});






