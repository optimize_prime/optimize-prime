/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, ScrollView, Image, TouchableOpacity, TextInput } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Rating } from 'react-native-ratings';
import NavBar from './navBar';
import ProductQuery from "./productQuery.js";


export default class searchScreen extends React.Component {
	constructor( ) {
			super();
			this.state = {
				searchQuery: "",
				resultsFound: false,
				errMsg: <Text></Text>,
			};
	}

	async search( ) {
		this.setState({resultsFound: false});
		this.setState({errMsg: <Text>Loading...</Text>});
		this.setState({nameProducts: <Text></Text>});
		this.setState({wineryProducts: <Text></Text>});
		this.setState({grapeProducts: <Text></Text>});


		this.setState({searchLoading: true});
		var query = this.state.searchQuery;
		var refinedQuery = query.replace(/\s+$/, '');
		this.setState({searchQuery: refinedQuery});

		// Query for all products matching specififc name
		let searchName = new ProductQuery( 'name', refinedQuery, this.props );
		let nameQuery = await searchName.queryResults();

		// Query for all products matching specific Winery
		let searchWinery = new ProductQuery( 'winery', refinedQuery, this.props );
		let wineryQuery = await searchWinery.queryResults();

		//Query for all products matching specific grape
		let searchGrape = new ProductQuery( 'grape', refinedQuery, this.props );
		let grapeQuery = await searchGrape.queryResults();

		if( nameQuery == 'No matching documents.' && wineryQuery == 'No matching documents.' && grapeQuery == 'No matching documents.')
		{
			this.setState({errMsg: <Text>No Results Found for "{this.state.searchQuery}"</Text>});
		}
		else {
			this.setState({resultsFound: true});

			if( nameQuery != 'No matching documents.')
				this.setState({nameProducts: nameQuery});
			if( wineryQuery != 'No matching documents.')
				this.setState({wineryProducts: wineryQuery});
			if( grapeQuery != 'No matching documents.')
				this.setState({grapeProducts: grapeQuery});
		}
		
	}

	render() {
		const nav = new NavBar(4, "featProduct", "Login", this.props);

		if( !this.state.resultsFound )
		{
			return(
				<View style={styles.container}>
					{nav.render()}
					
					<View style={styles.searchScrnCtnr}>
						<View style={styles.searchBarCtnr}>
							<TextInput
								placeholder="Search..."
								placeholderTextColor="#B0B0B0"
								style={styles.searchBar}
								returnKeyType='search'
								onChangeText={(text) => this.setState({searchQuery: text})}
								onSubmitEditing={ ({ event }) => this.search() }
							/>
						</View>

						<View style={styles.searchRsltCtnr}>
							<ScrollView>
								{this.state.errMsg}
							</ScrollView>
						</View>
					</View>
				</View>

			);
		}

		return(
			<View style={styles.container}>
				{nav.render()}
				
				<View style={styles.searchScrnCtnr}>
					<View style={styles.searchBarCtnr}>
						<TextInput
							placeholder="Search..."
							placeholderTextColor="#B0B0B0"
							style={styles.searchBar}
							returnKeyType='search'
							onChangeText={(text) => this.setState({searchQuery: text})}
							onSubmitEditing={ ({ event }) => this.search() }
						/>
					</View>

					<View style={styles.searchRsltCtnr}>
						<ScrollView>
							{this.state.nameProducts}
							{this.state.wineryProducts}
							{this.state.grapeProducts}
						</ScrollView>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  searchScrnCtnr: {
  	flex: 7,
  	flexDirection: 'column',
  },
  searchBarCtnr: {
  	flex: 1,
  },
  searchBar: {
  	flex: 1,
  	backgroundColor: '#F0F0F0',
  	borderWidth: 1,
  	borderColor: '#B0B0B0',
  	borderRadius: 10,
  	margin: 10,
  	paddingLeft: 5,
  },
  searchRsltCtnr: {
  	flex: 7,
  },
});






