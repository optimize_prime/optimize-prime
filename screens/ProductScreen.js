/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/

import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, ScrollView, Image, Button, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Rating } from 'react-native-ratings';
import NavBar from './navBar';
import AddCart from './AddToCart.js';

/* Author: Max Kiniklis
 * This file contains all of the components for rendering the product screen
**/

export default class ProductScreen extends React.Component {
	constructor( ) {
    super();
	}

	render() {
		const nav = new NavBar(4, "Product", "FeaturedProduct", this.props);  // Get the Nav bar from the navbar class

    // Load in all of the product data
    const ID = this.props.navigation.getParam('id', 0);
    const NAME = this.props.navigation.getParam('name', 'Error Loading Name');
    const IMGURL = this.props.navigation.getParam('imgURL', 'NA');
    const PRICE = this.props.navigation.getParam('price', 0.00);
    const RATING = this.props.navigation.getParam('rating', 0);
    const WINERY = this.props.navigation.getParam('winery', 'N/A');
    const VINTAGE = this.props.navigation.getParam('vintage', 'N/A');
    const GRAPE = this.props.navigation.getParam('grape', 'N/A');
    const ORIGIN = this.props.navigation.getParam('origin', 'N/A');
    //<Image source={{uri: IMGURL}} style={{width: 110, height: 150, resizeMode: "contain"}}  />

    // Display the product data 
		return(
			<View style={styles.container}>
				{nav.render()}

        <View style={styles.prodPageCtnr}>
          <View style={styles.scrollPageWrap}>
            <ScrollView style={styles.scrollPage}>
              <View style={styles.imgCtnr}>
                <Image source={{uri: IMGURL}} style={{height: 300, width: 500, resizeMode: "contain"}}  />
              </View>

              <View style={styles.prodInfoCtnr}>

                <View style={styles.prodNameCtnr}>
                  <Text style={styles.prodName}>{NAME}</Text>
                </View>

                <View style={styles.ratingPrice}>
                  <View style={styles.ratingCtnr}>
                    <Rating
                      type='star'
                      startingValue={RATING}
                      imageSize={30}
                      readonly={true}
                    />
                  </View>

                  <View style={styles.priceCtnr}>
                    <Text style={styles.price}>${PRICE}</Text>
                  </View>
                </View>

                <View style={styles.wineryCntr}>
                  <Text style={styles.wineInfo}>Winery: {WINERY}</Text>
                </View>

                <View style={styles.infoCntr}>
                  <Text style={styles.wineInfo}>Vintage: {VINTAGE}</Text>
                </View>

                <View style={styles.infoCntr}>
                  <Text style={styles.wineInfo}>Grape: {GRAPE}</Text>
                </View>

                <View style={styles.infoCntr}>
                  <Text style={styles.wineInfo}>Origin: {ORIGIN}</Text>
                </View>
        
              </View>
            </ScrollView>
          </View> 


          <View style={styles.botBarCtnr}>
            <View style={styles.addCartCtnr}>
              <TouchableOpacity style={styles.addToCartBtn} activeOpacity={.7} onPress={() => AddCart.addItem(ID,NAME,PRICE) }>
                <Text style={styles.addCartText}>Add To Cart</Text>
              </TouchableOpacity>
            </View>
          </View>

        </View>
				
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  prodPageCtnr: {
    flex: 7,
  },
  scrollPageWrap: {
    flex: 7,
    flexDirection: 'column',
  },
  scrollPage: {
    flex: 1,
    flexDirection: 'column',
  },
  imgCtnr: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1.5,
    borderColor: '#81151e',
  },
  prodInfoCtnr: {
    flex: 1,
    flexDirection: 'column',
    paddingBottom: 20,
  },
  prodNameCtnr: {
    flex: 2,
    paddingLeft: 5,
  },
  prodName: {
    flex: 1,
    fontSize: 30
  },
  ratingPrice: {
    flex: 1,
    flexDirection: 'row',
  },
  ratingCtnr:
  {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 5,
  },
  priceCtnr: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 5
  },
  price: {
    fontSize: 25,
  },
  wineryCntr: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 20,
    paddingLeft: 5,
  }, 
  infoCntr: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    paddingLeft: 5,
  },
  wineInfo: {
    fontSize: 20,
  },
  botBarCtnr: {
    flex: 1,
    flexDirection: 'row',
    bottom: 0,
    borderTopWidth: 1,
    borderColor: '#81151e',
    backgroundColor: '#FAFAFA',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addToCartBtn: {
    backgroundColor: '#81151e',
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 10,
  },
  addCartText: {
    color: '#FFFFFF',
    fontSize: 20,
  },


});
