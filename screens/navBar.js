/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Image, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

/* Author: Max Kiniklis
 * This class creates the components for the navbar and handles all of its functionality.
**/


export default class NavBar extends React.Component {

    state = {
        flagImage: true,
    }

	constructor( itemCnt, currPage, prevPage, props ) {
		super();
		this.itemCnt = itemCnt;
		this.currPage = currPage;
		this.prevPage = prevPage;
        this.props = props;

        this.state = {
            flagImage: true,
        };
	}

	incCount() {
        this.itemCnt++;
	}

	decCount() {
		this.itemCnt--;
	}

	pageChange( newPage ) {
		this.prevPage = this.currPage;
		this.currPage = newPage;
    }

	render() {
		return(
			<View style={styles.navContainer}>
				<View style={styles.navTopWrap}>
					<View style={styles.navTopBlock}></View>
					<View style={styles.navTopBlock}>
						<Image source={require('../assets/nav/logo.png')} style={styles.navLogoImg}/>
					</View>

					<View style={styles.navTopBlock}>
            <TouchableOpacity onPress = {() => this.props.navigation.navigate('CartScreen')}>
						  <Image source={require('../assets/nav/shoppingCart.png')} style={styles.navCartImg}/>
            </TouchableOpacity>
					</View>
				</View>

				<View style={styles.navBotWrap}>
					<View style={styles.navBotBlock}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
              <Image source={this.state.flagImage == true ? require('../assets/nav/homeRed.png') : require('../assets/nav/homeWhite.png') } style={styles.navBotImg}/>
            </TouchableOpacity>
					</View>

					<View style={styles.navBotBlock}>
            <TouchableOpacity onPress = {() => this.props.navigation.navigate('SearchScreen')}>
						  <Image source={require('../assets/nav/searchRed.png')} style={styles.navBotImg}/>
            </TouchableOpacity>
					</View>
					<View style={styles.navBotBlock}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Settings')}>
                            <Image source={require('../assets/nav/profRed.png')} style={styles.navBotImg} />
                        </TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}


const styles = StyleSheet.create({
  navContainer: {
    flex: 2,
    flexDirection: 'column',
    backgroundColor: '#fff',
    marginTop: 24,
  },
  navTopWrap: {
  	flex: 3,
  	flexDirection: 'row',
  },
  navTopBlock: {
  	flex: 1,
  	alignItems: 'center',
  	justifyContent: 'center',
  },
  navLogoImg:
  {
  	flex: 1,
  	width: 100,
  	height: 100,
  	resizeMode: "contain",
  },
  navCartImg:
  {
  	flex: 1,
  	width: 30,
  	height: 30,
  	resizeMode: "contain",
  },
  navBotWrap: {
  	flex: 2,
  	flexDirection: 'row',
  },
  navBotBlock: {
  	flex: 1,
  	borderWidth: 2,
  	borderColor: '#81151e',
  	justifyContent: 'center',
  	alignItems: 'center',
  },
  navBotImg: {
  	flex: 1,
  	width: 40,
  	height: 40,
    resizeMode: "contain",
    },
navBotHomeImg: {
    flex: 1,
    width: 40,
    height: 40,
    resizeMode: "contain",
    },
  highlighted: {
  	flex: 1,
  	backgroundColor: '#81151e',
    borderWidth: 2,
  	borderColor: '#81151e',
  },

});
