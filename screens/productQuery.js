/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, ScrollView, Image, TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Rating } from 'react-native-ratings';
import Firebase from './firebase.js';



export default class ProductQuery extends React.Component {

	constructor( queryCat, queryVal, prop ) {
		super();
		this.state = { 
  			isLoaded: false,
  			queryCategory: queryCat,
  			query: queryVal, 
		};

		this.props = prop;
		// On constructor call,
		// ***Take in the Query 
		// Query the database and return all objects that match
		// For each object, put it in the correct format
		// Return an array of "product" components

	}

  		// This function should take in the query value, query the DB and return an array of product components
  		async queryResults( ) {	
  			var results = [];	
  			var notFound = false;
  			//Query Firebase
  			let prodRef = Firebase.firestore().collection('Products');

  			// Search the database for a product and return each result as a clickable element
  			let query = await prodRef.where( this.state.queryCategory, '==', this.state.query ).get()
  				.then(snapshot => {
				    if (snapshot.empty) {
				      console.log('No matching documents.');
				      notFound = true;
				      return;
				    }  
				    snapshot.forEach(prod => {
				      results.push(  
				      	<TouchableOpacity onPress = {() => this.props.navigation.navigate('ProductScreen', {
							id: prod.id, 
							name: prod.data().name, 
							imgURL: prod.data().imageURL,
							price: prod.data().price,
							rating: prod.data().ratingVal,
							winery: prod.data().winery,
							vintage: prod.data().vintage,
							grape: prod.data().grape,
							origin: prod.data().origin,
						})} style={styles.productContainer}>
							<View style={styles.prodImgCont}>
								<Image source={{uri: prod.data().imageURL}} style={{width: 110, height: 150, resizeMode: "contain"}}  />
							</View>

							<View style={styles.prodInfoCont}>
								<View style={styles.prodInfoRow}>
										<Text style={{fontSize: 15}}>{prod.data().name}</Text>
								</View>


								<View style={styles.prodInfoRow}>
									<Rating
										type="star"
										startingValue={prod.data().ratingVal}
										imageSize={20}
										readonly={true}
										style={styles.prodRating}
									/>
								</View>


								<View style={styles.prodInfoBotRow}>
									<View style={styles.prodInfoColRow}>
									</View>

									<View style={styles.prodInfoColRow}>
										<Text style={{fontSize: 30}}>${prod.data().price}</Text>
									</View>
								</View>
							</View>
						</TouchableOpacity>
					  );
				    });
			  	})
				.catch(err => {
			    	console.log('Error getting documents', err);
				});

			if( notFound )
			{
				return 'No matching documents.';
			}	

  			return(
  				results
			);
  		}
}

const styles = StyleSheet.create({

  productContainer: {
  	flex: 1,
  	flexDirection: 'row',
  	borderWidth: 1.5,
  	borderColor: '#81151e',
  	margin: 10,
  },
  prodImgCont: {
  	flex: 1,
  	alignItems: 'center',
  	justifyContent: 'center',
  	borderRightWidth: 1.5,
  	borderColor: '#81151e',
  },
  prodInfoCont: {
	flex: 2,
	flexDirection: 'column',
  },
  prodInfoCol: {
  	flex: 1,
  	flexDirection: 'column'
  },
  prodInfoRow:
  {
  	flex: 1,
  },
  prodInfoBotRow: {
  	flex: 1,
  	flexDirection: 'row'
  },
  prodInfoColRow: {
  	flex: 1,
  	alignItems: 'flex-end',
  	justifyContent: 'flex-end',
  	paddingRight: 5,
  },
  prodRating: {
  	flex: 1,
  	width: 100,
  }
});

