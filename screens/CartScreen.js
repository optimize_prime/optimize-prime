/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, ScrollView, Image, Button, TouchableOpacity, TextInput } from 'react-native';
import Fire from '.././Firebase/Fire.js';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import NavBar from './navBar';

/* Author: Max Kiniklis
 * This file contains all of the components for rendering the product screen
**/

/*---------Cart Issues---------------
 * Only updates the cart with the most recent addition
 *  ------> Firebase however is still updated with all items in cart
 *
 *  Upon Deletion of an item, the item will not be removed from the screen.
 *  ------> Firebase however updates the deletion of the cart, and will not be in the User's cart
 *
 *  Pricing of the items in cart does not function well beacuse of the previous issues.
 * 
 **/
export default class CartScreen extends React.Component {
   
	constructor( ) {
    super();
    }

    state = {
        product: {},
        products: {},
        PID: {
            ProductID: '1'
        }

    };


    //When component mounts user will grab data from firebase UserProfile table
    componentDidMount() {

        this.getProduct();
    }

    getProduct() {
        const user = this.props.uid || Fire.shared.uid;
        let productRef = Fire.shared.firestore.collection('UserProfile').doc(Fire.shared.uid).collection('Cart').doc(Fire.shared.uid).collection('Products');
        let query = productRef.get()
    .then(snapshot => {
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }

        snapshot.forEach(doc => {
            this.setState({ products: doc.data() });
            this.setState({ PID: doc.data() });
        });
    })
    .catch(err => {
        console.log('Error getting documents', err);
    });

    }

    getAnotherProduct() {
        let productRef = Fire.shared.firestore.collection('UserProfile').doc(Fire.shared.uid).collection('Cart').doc(Fire.shared.uid).collection('Products');
        let query = productRef.get()
            .then(snapshot => {
                if (snapshot.empty) {
                    console.log('No matching documents.');
                    return;
                }

                snapshot.forEach(doc => {
                    this.setState({ product: doc.data() });
                });
            })
            .catch(err => {
                console.log('Error getting documents', err);
            });
    }

    removeProduct(PID) {

        // Create a document reference

       // let deleteProduct = Fire.shared.firestore.collection('UserProfile').doc(Fire.shared.uid).collection('Cart').doc(Fire.shared.uid).collection('Products').doc(PID).delete();
    }

	render() {
        const nav = new NavBar(4, "featProduct", "Login", this.props);
       
		return(
			<View style={styles.container}>
				{nav.render()}
        <View style={styles.cartPageCtnr}>
          
          {/* Cart Header Banner */}
          <View style={styles.cartPageHdrCtnr}>
            <Text style={styles.cartPageHdrTxt}>Cart</Text>
          </View>

          <View style={styles.cartProdListCtnr}>
            <ScrollView>
              <View style={styles.cartProdListHdrCtnr}>
                <View style={styles.cartProdListHdrName}>
                  <Text style={styles.cartProdListHdrTxt}>Items</Text>
                </View>

                <View style={styles.cartProdListHdrPrice}>
                  <Text style={styles.cartProdListHdrTxt}>Price</Text>
                </View>

                <View style={styles.cartProdListHdrQty}>
                  <Text style={styles.cartProdListHdrTxt}>Qty</Text>
                </View>
              </View>

              <View style={styles.cartProdListScroll}>

                <View style={styles.cartProdListScrollProd}>
                  <View style={styles.cartProdListScrollProd_NameCtnr}>
                    <Text>{this.state.products.ProductName}</Text>
                  </View>

                  <View style={styles.cartProdListScrollProd_PriceCtnr}>
                    <Text>${this.state.products.ProductPrice}</Text>
                  </View>

                  <View style={styles.cartProdListScrollProd_QtyCtnr}>
                    <TextInput
                      placeholder="1"
                      placeholderTextColor="#B0B0B0"
                      style={styles.cartProdQtyInput}
                      keyboardType='numeric'
                      returnKeyType='done'
                    />
                  </View>
                <View style={styles.cartProdListScrollProd_DelCtnr}>
                    <TouchableOpacity style={styles.cartProdDelBtn} onPress={this.removeProduct(this.state.PID.ProductID)}>
                      <Text style={{fontSize: 20}}>X</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.cartProdListScrollProd}>
                  <View style={styles.cartProdListScrollProd_NameCtnr}>
                   <Text>{this.state.product.ProductName}</Text>
                  </View>

                  <View style={styles.cartProdListScrollProd_PriceCtnr}>
                   <Text>${this.state.product.ProductPrice}</Text>
                  </View>

                  <View style={styles.cartProdListScrollProd_QtyCtnr}>
                    <TextInput
                      placeholder="1"
                      placeholderTextColor="#B0B0B0"
                      style={styles.cartProdQtyInput}
                      keyboardType='numeric'
                      returnKeyType='done'
                    />
                  </View>

                  <View style={styles.cartProdListScrollProd_DelCtnr}>
                    <TouchableOpacity style={styles.cartProdDelBtn} onPress={this.removeProduct(this.state.PID.ProductID)}>
                      <Text style={{ fontSize: 20 }}>X</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
              <View style={styles.cartPriceCtnr}>
                <View style={styles.cartSubTotalCtnr}>
                  <View style={styles.cartTotalSmallCol}>
                    <Text style={styles.cartPriceSmall}>Subtotal:</Text>
                  </View>
                  <View style={styles.cartTotalSmallCol}>
                   <Text>${this.state.products.ProductPrice}</Text>
                  </View>
                  <View style={styles.cartTotalBigCol}></View>
                </View>

                <View style={styles.cartTaxCtnr}>
                  <View style={styles.cartTotalSmallCol}>
                    <Text style={styles.cartPriceSmall}>Tax:</Text>
                    </View>
                // Calculating Price
                  <View style={styles.cartTotalSmallCol}>
                   <Text>${((this.state.products.ProductPrice) * .065).toFixed(2)}</Text>
                  </View>
                  <View style={styles.cartTotalBigCol}></View>
                </View>

                <View  style={styles.cartTotalCtnr}>
                  <View style={styles.cartTotalSmallCol}>
                    <Text style={styles.cartPriceLabelBig}>Total:</Text>
                    </View>
                // Calculating total price
                  <View style={styles.cartTotal3Col}>
                   <Text style={styles.cartPriceBig}>${((this.state.products.ProductPrice * .065)+(this.state.products.ProductPrice)).toFixed(2)}</Text>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>

          <View style={styles.cartChkoutCtnr}>
                        <TouchableOpacity style={styles.cartChkoutBtn} activeOpacity={.7} onPress = {() => this.props.navigation.navigate('CheckoutScreen')}>
              <Text style={styles.cartChkoutBtnTxt}>Checkout</Text>
            </TouchableOpacity>
          </View>


        </View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  cartPageCtnr: {
    flex: 7,
    flexDirection: 'column',
  },
  cartPageHdrCtnr: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartPageHdrTxt: {
    fontSize: 30,
    color: '#81151e',
  },
  cartProdListCtnr: {
    flex: 6,
    flexDirection: 'column',
  },
  cartProdListHdrCtnr: {
    height: 50,
    flexDirection: 'row',
  },
  cartProdListHdrName: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
  },
  cartProdListHdrPrice: {
    flex: 1.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
  },
  cartProdListHdrQty: {
    flex: 1.5,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: 5,
  },
  cartProdListHdrTxt: {
    color: '#81151e',
    fontWeight: 'bold',
    borderBottomColor: '#81151e',
    borderBottomWidth: 1,
  },
  cartProdListScroll: {
    flexDirection: 'column',
  },
  cartPriceCtnr: {
    height: 140,
    flexDirection: 'column',
    borderTopWidth: 1,
    borderTopColor: '#81151e',
    borderRadius: 20,
    paddingTop: 10,
    marginBottom: 10,
  },
  cartSubTotalCtnr: {
    flex: 1,
    flexDirection: 'row',
  },
  cartTaxCtnr: {
    flex: 1,
    flexDirection: 'row',
  },
  cartTotalCtnr: {
    flex: 2,
    flexDirection: 'row',
  },
  cartTotalSmallCol: {
    flex: 1,
    paddingLeft: 5,
    justifyContent: 'center',
  },
  cartTotalBigCol: {
    flex: 2,
  },
  cartTotal3Col: {
    flex: 3,
    justifyContent: 'center',
  },
  cartPriceSmall: {
    color: '#81151e',
    fontWeight: 'bold',
  },
  cartPriceLabelBig: {
    color: '#81151e',
    fontWeight: 'bold',
    fontSize: 30,
  },
  cartPriceBig: {
    fontSize: 30,
  },
  cartChkoutCtnr: {
    flex: 1,
    flexDirection: 'row',
    bottom: 0,
    borderTopWidth: 1,
    borderColor: '#81151e',
    backgroundColor: '#FAFAFA',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cartChkoutBtn: {
    backgroundColor: '#81151e',
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 10,
  },
  cartChkoutBtnTxt: {
    color: '#FFFFFF',
    fontSize: 20,
  },




  // Cart List Components
  cartProdListScrollProd: {
    flex: 1,
    flexDirection: 'row',
    height: 80,
    marginTop: 10,
  },
  cartProdListScrollProd_NameCtnr: {
    flex: 2,
    paddingLeft: 5,
    paddingRight: 10,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  cartProdListScrollProd_PriceCtnr: {
    flex: 1.5,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  cartProdListScrollProd_QtyCtnr: {
    flex: 0.75,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  cartProdListScrollProd_DelCtnr: {
    flex: 0.75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartProdQtyInput: {
    flex: 1,
    backgroundColor: '#F0F0F0',
    borderWidth: 1,
    borderColor: '#B0B0B0',
    margin: 10,
    paddingLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
