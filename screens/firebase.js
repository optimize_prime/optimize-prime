/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/

import firebase from 'firebase'
import '@firebase/firestore';

/************************
This file contains all of the required information to connect the application to Firebase
************************/
const firebaseConfig = {
	apiKey: "AIzaSyAV9D60MlNB7LZp_yR4REvcgerMUVGQ-Kw",
	authDomain: "capstone-4510b.firebaseapp.com",
	databaseURL: "https://capstone-4510b.firebaseio.com",
	projectId: "capstone-4510b",
	storageBucket: "capstone-4510b.appspot.com",
	messagingSenderId: "472186844813",
	appId: "1:472186844813:web:63e7d7d3c0f233ec6ed4c4",
	measurementId: "G-527G1JSLB8"
};


//const Firebase = firebase.firestore();

export default firebase.initializeApp(firebaseConfig);;