/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React, { Component } from 'react';
import { StyleSheet, Button, View, Card, Image, Text, TouchableOpacity, StatusBar, ScrollView, KeyboardAvoidingView } from 'react-native';
import Fire from '../../Firebase/Fire';

/* Author: Dante DeSantis
 * This class will appear after the ConfirmationScreen and will gather data about how the User
 * interacts with the EULA.
 * This class contains functions that communicate with the Database.
**/
export default class EULA extends Component {

    // state.eula will store information about the way the user interracted with the EULA.
    state = {
        eula: {
            flag: 0,

        }
    };

    // handleAgreement => Calls another method updateAgreement() in Fire.js that will store the values of state.eula
    // Once the updateAgreement method is complete, the app will navigate the user to the Home screen.
    // After the user hits agree this function will be called updating the database and navigate
    // to the next screen which is the Home screen.
    handleAgreement = async () => {
        await Fire.shared.updateAgreement();
        this.props.navigation.navigate('Home')

    }

    // handleScroll => Calls another method onScroll() in Fire.js that will update whether the user read the EULA.
    handleScroll = async () => {
        await Fire.shared.onScroll();
    }
    // handleScroll will be called when the user scrolls over the EULA and will update the database
    handleScroll = async () => {
        await Fire.shared.onScroll(this.state.eula);


    }
    render() {
        return (
            <KeyboardAvoidingView behavior="position" style={styles.otherContainer}>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('./resources/logo.png')}
                    />
                </View>
                <View style={styles.container}>
                    <StatusBar
                        barStyle="dark-content"
                    />
                    <Text style={styles.title}>End User License Agreement</Text>
                    <ScrollView style={styles.scrollView} onScrollEndDrag={this.handleScroll}>
                        <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam suscipit mollis libero. Nam posuere enim at efficitur iaculis. Pellentesque iaculis egestas porta. Maecenas faucibus nisi at odio rutrum feugiat. Curabitur ac ante luctus, placerat augue non, cursus lorem. Integer feugiat non dui vel semper. Aenean lacinia, neque eget interdum tincidunt, lacus felis egestas magna, non porttitor nulla metus eget lorem. Integer sed quam blandit urna pellentesque lobortis. Duis nec metus nunc. Donec porta enim risus. Donec vestibulum interdum pretium. Aliquam eget tincidunt ex, eget ultrices augue. Suspendisse quis magna porttitor, cursus odio ac, bibendum velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
    
                        <Text style={{ fontSize: 18 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam suscipit mollis libero. Nam posuere enim at efficitur iaculis. Pellentesque iaculis egestas porta. Maecenas faucibus nisi at odio rutrum feugiat. Curabitur ac ante luctus, placerat augue non, cursus lorem. Integer feugiat non dui vel semper. Aenean lacinia, neque eget interdum tincidunt, lacus felis egestas magna, non porttitor nulla metus eget lorem. Integer sed quam blandit urna pellentesque lobortis. Duis nec metus nunc. Donec porta enim risus. Donec vestibulum interdum pretium. Aliquam eget tincidunt ex, eget ultrices augue. Suspendisse quis magna porttitor, cursus odio ac, bibendum velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                ittis orci ut nunc laoreet dapibus. Aenean eget gravida velit. Pellentesque mattis massa elit, vitae lobortis diam sodales nec. Fusce auctor nisl eget nunc commodo, eget sagittis nisi volutpat. Nam suscipit felis commodo arcu tempus, sit amet rhoncus nulla feugiat. Mauris aliquet fermentum nisi. Donec imperdiet porta nunc eu scelerisque. Proin dapibus enim eu purus pulvinar euismod. Vestibulum fermentum faucibus viverra. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec quis magna eu dolor condimentum viverra condimentum vel libero.
                    </Text>
                        </Text>
                    </ScrollView>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity onPress={() => this.handleAgreement()} style={styles.button} > //handleAgreement() method call on button press
                            <Text style={styles.buttonText}>Agree</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')} style={styles.button}> // navigates user to the login page when EULA is declined
                            <Text style={styles.buttonText}>Decline</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        marginTop: 30,
        paddingHorizontal: '10%',
    },

    buttonContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    scrollView: {
        height: "72%",
        marginTop: -15,
        marginBottom: 10,
    },

    button: {
        width: '48%',
        height: 50,
        borderRadius: 12,
        borderWidth: 0,
        backgroundColor: '#800000',
    },

    buttonText: {
        marginTop: 10,
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 20
    },

    logoContainer: {
        marginTop: 15,
        flexGrow: 1,
    },

    logo: {
        width: 125,
        height: 85,
        marginLeft: 125,
        marginBottom: -20
    },
    title: {
        color: '#800000',
        textAlign: 'center',
        marginTop: 5,
        fontSize: 25,
        fontWeight: '400',
        marginBottom: 25,
        paddingHorizontal: 15
    },
});
