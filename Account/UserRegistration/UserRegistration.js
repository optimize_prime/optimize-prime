/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React, { Component } from 'react';
import { StyleSheet, Button, ScrollView, TextInput, View, Image, Text, TouchableOpacity, StatusBar, KeyboardAvoidingView } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import Fire from '../../Firebase/Fire';
import ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import Permissions from 'expo-permissions';

// Type of wines to select from in the wine-picker durring registration
/* Author: Dante DeSantis
 * This class will appear when the user wants to make an account.
 * The user will provide information about themselves and store the information in the UserProfile table.
 * This class contains functions that communicate with the Database.
**/

// Wines for Picker component

const wines = [
    {
        label: 'Chardonnay',
        value: 'chardonnay',
    },

    {
        label: 'Pinot Noir',
        value: 'pinot noir'
    },

    {
        label: 'Merlot',
        value: 'merlot'
    },


];

export default class UserRegistration extends Component {

    // User state that updates all varibales when assigned in order to pass values to firebase later
    state = {
        // User that stores all the information about the user

        user: {
            fName: '',
            lName: '',
            email: '',
            password: '',
            zipCode: '',
            favWine: '',
            date: '',
            accept: 0,
            decline: 0,
            credit: 10.00,
            image: null,

        },
        image: null,
        errorMessage: null
    };

    // handleSignUp => takes the current user and associated values calls the addUser method in Fire.js
    // in order to store values to firebase.
    // Next, the method will navigate the user to the EULA screen upon registration completion.
    // handleSignUp will connect to the firebase to add the User after registration

    handleSignUp = async () => {
        await Fire.shared.addUser(this.state.user);
        this.props.navigation.navigate('ConfirmationScreen')
    };

    // componentDidMount => Upon component mount, the method getPhotoPermission will be called to ask
    // the user for permission to access their camera functionality.
    componentDidMount() {
        this.getPhotoPermission();
    }

    // getPhotoPermission => Creates a spalsh box asking the user for permission to their camera / camera_roll
    // if the permission is not granted a dialog box will pop up indicating that they declined.
    getPhotoPermission = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

            if (status != "granted") {
                alert("We need permission to use your camera roll if you'd like to incude a photo.");
            }
        }
    };

    // pickImage => Waits for the user's camera roll to appear and then updates the user.img value accordingly.
    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3]
        });

        if (!result.cancelled) {
            this.setState({ image: result.uri });
        }
    };


    render() {
        // place holder for the wine-selector

        // Placeholder for the wine picker

        const placeholder = {
            label: 'Select a wine',
            value: null,
            color: '#9EA0A4',
        };

        return (
            <ScrollView style={styles.scrollView}>
                <KeyboardAvoidingView behavior="padding" style={styles.otherContainer}>
                    <View style={styles.logoContainer}>
                        <Image                          // Logo Image
                            style={styles.logo}
                            source={require('./resources/logo.png')}
                        />
                    </View>
                    <View style={styles.container}>
                        <StatusBar
                            barStyle="dark-content"
                        />
                        <View style={styles.errorMessage}>
                            {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                        </View>
                        <TextInput
                            placeholder="* First Name"
                            returnKeyType="next"
                            onSubmitEditing={() => this.lNameInput.focus()}
                            autoCapitalize="true"
                            autoCorrect={false}
                            style={styles.input}
                            onChangeText={fName => this.setState({ user: { ...this.state.user, fName } })}
                            value={this.state.user.fName}
                        />
                        <Image
                            style={styles.line}
                            source={require('./resources/line.png')}
                        />
                        <TextInput
                            placeholder="* Last Name"
                            returnKeyType="next"
                            autoCapitalize="true"
                            autoCorrect={false}
                            style={styles.input}
                            ref={(input) => this.lNameInput = input}
                            onSubmitEditing={() => this.emailInput.focus()}
                            onChangeText={lName => this.setState({ user: { ...this.state.user, lName } })}
                            value={this.state.user.lName}
                        />
                        <Image
                            style={styles.line}
                            source={require('./resources/line.png')}
                        />
                        <TextInput
                            placeholder="* Email"
                            returnKeyType="next"
                            keyboardType="email-address"
                            autoCapitalize="none"
                            autoCorrect={false}
                            style={styles.input}
                            ref={(input) => this.emailInput = input}
                            onSubmitEditing={() => this.passwordInput.focus()}
                            // upon text change the user.email will update
                            onChangeText={email => this.setState({ user: { ...this.state.user, email } })}
                            value={this.state.user.email}
                        />
                        <Image
                            style={styles.line}
                            source={require('./resources/line.png')}
                        />
                        <TextInput
                            placeholder="* Password"
                            returnKeyType="go"
                            secureTextEntry
                            style={styles.input}
                            // upon text change the user.password will update
                            onChangeText={password => this.setState({ user: { ...this.state.user, password } })}
                            value={this.state.user.password}
                            ref={(input) => this.passwordInput = input}
                        />
                        <Image
                            style={styles.line}
                            source={require('./resources/line.png')}
                        />
                        <View style={styles.subTextContainer}>
                            <Text style={styles.subTitle}>Date of Birth</Text>
                        </View>
                        <DatePicker         // DatePicker intialization
                            style={{ width: 265, marginBottom: 20 }}
                            date={this.state.date}
                            mode="date"
                            placeholder="Select date"
                            format="MM-DD-YYYY"
                            minDate="01-01-1950"
                            maxDate="12-30-2019"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0,
                                },
                                dateInput: {
                                    marginLeft: 36,
                                },
                            }}
                            // when the user changes the date the user.date will update
                            onDateChange={date => this.setState({ user: { ...this.state.user, date }, date: date })}
                            value={this.state.user.date}
                        />
                        <TextInput
                            placeholder="ZIP Code"
                            returnKeyType="done"
                            autoCorrect={false}
                            style={styles.input}
                            ref={(input) => this.zipInput = input}
                            // upon text change the user.zipCode will be updated
                            onChangeText={zipCode => this.setState({ user: { ...this.state.user, zipCode } })}
                            value={this.state.user.zipCode}
                        />
                        <Image
                            style={styles.line}
                            source={require('./resources/line.png')}
                        />
                        <View style={styles.subTextContainer}>
                            <Text style={styles.subTitle}>Upload Profile Picture</Text>
                        </View>
                        <TouchableOpacity style={styles.imageButtonContainer} onPress={this.pickImage}>
                            <Text style={styles.imageButtonText}>Select Image</Text>
                        </TouchableOpacity>
                        <View style={styles.subTextContainer}>
                            <Text style={styles.subTitle}>Favorite Type of Wine</Text>
                            <RNPickerSelect     // Wine Picker initilaization
                                placeholder={placeholder}
                                items={wines}
                                onValueChange={favWine => this.setState({ user: { ...this.state.user, favWine } })}
                                onUpArrow={() => {
                                    this.zipInput.focus();
                                }}
                                onDownArrow={() => {
                                }}
                                style={pickerSelectStyles}
                                value={this.state.user.favWine}
                            />
                        </View>
                        // When the user clicks 'Create Account' the handleSignUp() method will be called.
                        <TouchableOpacity onPress={this.handleSignUp} style={styles.buttonContainer}>
                            <Text style={styles.buttonText}>Create Account</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        marginTop: 30,
        paddingHorizontal: 5,
    },

    scrollView: {
        height: "80%",
        marginTop: -15,
        marginBottom: 10,
    },


    line: {
        height: 10,
        width: 266,
        color: 'black',
        marginBottom: 25
    },

    imageButtonContainer: {
        paddingVertical: 8,
        marginRight: 145,
        marginBottom: 20,
        marginTop: 0,
        borderRadius: 12,
        borderWidth: 0,
        backgroundColor: '#800000'
    },

    createButtonContainer: {
        paddingVertical: 14,
        marginTop: 10,
        borderRadius: 12,
        borderWidth: 0,
        backgroundColor: '#800000'
    },

    imageButtonText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 15
    },

    createButtonText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 25
    },

    buttonContainer: {
        paddingVertical: 12,
        marginBottom: 20,
        marginTop: 15,
        borderRadius: 7,
        borderWidth: 0,
        backgroundColor: '#800000'
    },

    buttonText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 20
    },

    logoContainer: {
        marginTop: 15,
        flexGrow: 1,
    },

    logo: {
        width: 135,
        height: 80,
        marginLeft: 70,
    },

    otherContainer: {
        backgroundColor: '#fff',
        paddingHorizontal: 50
    },

    textContainer: {
        marginBottom: 20,
    },

    subTextContainer: {
        marginTop: -10,
        marginBottom: 5,
    },

    title: {
        color: '#800000',
        textAlign: 'center',
        fontSize: 27
    },

    subTitle: {
        color: '#800000',
        textAlign: 'left',
        // fontWeight: 598,
        fontSize: 20,
        marginBottom: 5,
    },

    error: {
        color: '#E9446A',
        fontSize: 13,
        fontWeight: '600',
        textAlign: 'center'
    },

    errorMessage: {
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 30,
    }


});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 14,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#800000',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30,
    },

    inputAndroid: {
        fontSize: 14,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: '#800000',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30,
    },
});