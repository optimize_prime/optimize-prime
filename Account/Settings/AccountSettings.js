/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, ScrollView, Image, TouchableOpacity, TextInput } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Rating } from 'react-native-ratings';
import NavBar from '../.././screens/navBar';
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import Fire from '../.././Firebase/Fire.js';
import * as firebase from 'firebase';

/* Author: Dante DeSantis
 * This class shows the information about the User. Can be navigated to by pressing the navBar icon.
 * Also allows the user to change their profile picture.
 * This class contains functions that communicate with the Database.
**/

const camera = Permissions.CAMERA_ROLL;

export default class AccountSettings extends React.Component {

    state = {
        user: {},
    };


    //When component mounts user will grab data from firebase UserProfile table
    componentDidMount() {
        const user = this.props.uid || Fire.shared.uid;

        this.unsubscribe = Fire.shared.firestore.collection('UserProfile').doc(user).onSnapshot(doc => {
            this.setState({ user: doc.data() });
        });
    }

    // Will sign out the user using firebase authentication

    signOutUser = () => {
        firebase.auth().signOut();
    }

    async getPhotoPermission() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        if (status != "granted") {
            alert("We need permission to use your camera roll if you'd like to incude a photo.");
        }

    };

    // pickImage will allow the user to pick an image from camera roll and add it to their profile.
    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3]
        });

        if (!result.cancelled) {
            this.setState({ image: result.uri });
            Fire.shared.addPost({ localUri: this.state.image });
        }


    };


    render() {
        // NavBar decleration to display the Navigation Bar at the top of the screen.
        const nav = new NavBar(4, "featProduct", "Login", this.props);
        this.getPhotoPermission();
        const { image } = this.state;
        return (
            <View style={styles.container}>
                {nav.render()}
                <View style={styles.prodPageCtnr}>
                    <View style={styles.scrollPageWrap}>
                        <ScrollView style={styles.scrollPage}>
                            <View style={styles.infoDisplay}>
                                <TouchableOpacity style={styles.avatarContainer} onPress={this.pickImage}>
                                    <Image style={styles.avatar}
                                        source={
                                            this.state.user.image
                                                ? { uri: this.state.user.image }
                                                : require('../.././assets/tempAvatar.jpg')
                                        }
                                    />
                                </TouchableOpacity>
                                <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 20, marginBottom: 50 }}>{this.state.user.fName} {this.state.user.lName}</Text>
                                <Text style={styles.emailLabel}>Email:</Text>
                                <Text style={styles.nameInfo}>{this.state.user.email}</Text>
                                <Image
                                    style={styles.line}
                                    source={require('./resources/line.png')}
                                />
                                <Text style={styles.wineLabel}>Favorite Wine:</Text>
                                <Text style={styles.wineInfo}>{this.state.user.alcoholPreference}</Text>
                                <Image
                                    style={styles.line}
                                    source={require('./resources/line.png')}
                                />
                                <Text style={styles.creditLabel}>Credit:</Text>
                                <Text style={styles.creditInfo}>{this.state.user.credit}</Text>
                                <Image
                                    style={styles.line}
                                    source={require('./resources/line.png')}
                                />
                                <TouchableOpacity style={{ marginTop: 50 }} onPress={this.signOutUser}>
                                    <Text style={styles.logout}>Logout</Text>
                                </TouchableOpacity>
                                <View style={{ marginHorizontal: 32, marginTop: 32, height: 150 }}>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'center',
    },

    prodPageCtnr: {
        flex: 7,
    },
    scrollPageWrap: {
        flex: 7,
        flexDirection: 'column',
    },
    scrollPage: {
        flex: 1,
        flexDirection: 'column',
    },

    infoDisplay: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    avatar: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatarContainer: {
        shadowColor: '#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4,
    },
    nameInfo: {
        flex: 1,
        marginTop: -17,
        justifyContent: 'center'
    },

    emailLabel: {
        flex: 1,
        marginLeft: -235,
        marginTop: 20,
        fontWeight: 'bold'
    },

    wineLabel: {
        flex: 1,
        marginLeft: -178,
        marginTop: 20,
        fontWeight: 'bold'
    },

    wineInfo: {
        flex: 1,
        marginTop: -17,
        justifyContent: 'center'
    },

    creditLabel: {
        flex: 1,
        marginLeft: -230,
        marginTop: 20,
        fontWeight: 'bold'
    },

    creditInfo: {
        flex: 1,
        marginTop: -17,
        justifyContent: 'center'
    },

    logout: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#800000',
    }




});











