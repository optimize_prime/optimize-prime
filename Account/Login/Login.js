/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React, { Component } from 'react';
import { StyleSheet, TextInput, View, ScrollView, Image, Text, TouchableOpacity, StatusBar, KeyboardAvoidingView } from 'react-native';
import firebase from 'firebase';

/* Author: Dante DeSantis
 * This class contains functions that communicate with the Database.
**/

export default class Login extends Component {
    // state stores the values the user provides in order to login
    state = {
        email: "",
        password: "",
        errorMessage: null
    };

    // handleLogin => Accesses the firebase in order to see if the user already exists in the database.
    // If they do it will log the user in. If not it will communicate with the user that the profile doesn't exist.

    // Create a user based on text inputs for email and password with firebase authentication

    handleLogin = () => {
        const { email, password } = this.state

        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .catch(error => this.setState({ errorMessage: error.message }));
    };

    render() {
        return (
            <View style={styles.scrollPageWrap}>
                <ScrollView style={styles.scrollPage}>
                    <KeyboardAvoidingView behavior="position" style={styles.otherContainer}>
                        <View style={styles.logoContainer}>
                            <Image
                                style={styles.logo}
                                source={require('./resources/logo.png')}
                            />
                        </View>
                        <View style={styles.container}>
                            <StatusBar
                                barStyle="dark-content"
                            />
                            <View style={styles.errorMessage}>
                                {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                            </View>
                            <TextInput
                                placeholder="Username"
                                returnKeyType="next"
                                onSubmitEditing={() => this.passwordInput.focus()}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                autoCorrect={false}
                                style={styles.input}
                                // on text change the state.email will be updated
                                onChangeText={email => this.setState({ email })}
                                value={this.state.email}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <TextInput
                                placeholder="Password"
                                returnKeyType="go"
                                secureTextEntry
                                style={styles.input}
                                // on text change the state.password will be updated
                                onChangeText={password => this.setState({ password })}
                                value={this.state.password}
                                ref={(input) => this.passwordInput = input}
                            />
                            <Image
                                style={styles.line}
                                source={require('./resources/line.png')}
                            />
                            <TouchableOpacity onPress={this.handleLogin} style={styles.buttonContainer}> // handleLogin() method call on button press
                                <Text style={styles.buttonText}>Login</Text>
                            </TouchableOpacity>
                            <View style={styles.textContainer}>
                                <Text style={styles.title}>Don't have an account?</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserRegistration')} style={styles.buttonContainer}> // navigates user to UserRegistation screen on button press
                                <Text style={styles.buttonText}>Create Account</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        marginTop: 50,
        paddingHorizontal: 5,
    },

    input: {
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, 0)',
        paddingHorizontal: 2
    },


    line: {
        height: 10,
        width: 266,
        color: 'black',
        marginBottom: 10
    },

    buttonContainer: {
        paddingVertical: 12,
        marginBottom: 20,
        marginTop: 15,
        borderRadius: 7,
        borderWidth: 0,
        backgroundColor: '#800000'
    },

    buttonText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 20
    },

    textContainer: {
        marginTop: 5,
        marginBottom: -13,
    },

    title: {
        color: '#a9a9a9',
        textAlign: 'center'
    },
    otherContainer: {
        backgroundColor: '#fff',
        paddingHorizontal: 50
    },

    logoContainer: {
        marginTop: 75,
        flexGrow: 1,
    },

    logo: {
        width: 250,
        height: 179,
        marginLeft: 20,
    },

    error: {
        color: '#E9446A',
        fontSize: 13,
        fontWeight: '600',
        textAlign: 'center'
    },

    errorMessage: {
        height: 72,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 30,
    },

    scrollPageWrap: {
        flex: 7,
        flexDirection: 'column',
    },
    scrollPage: {
        flex: 1,
        flexDirection: 'column',
    },


});
