/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, StatusBar} from 'react-native';


/* Author: Dante DeSantis
 * This is the last screen the User will see and will state the purpose of the app and
 * inform the user that no billing information was saved and no purchase was made.
**/

export default class Thankyou extends React.Component {
s
    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.scrollView}>
                    <View style={styles.logoContainer}>
                        <Image
                            style={styles.logo}
                            source={require('./resources/logo.png')}
                        />
                    </View>
                    <View style={styles.container}>
                        <StatusBar
                            barStyle="dark-content"
                        />
                    </View>
                    <Text style={styles.title}>Thank you for your participation!</Text>
                    <View style={styles.message}>
                        <Text style={styles.messageHeader}>Dear User,</Text>
                        <Text style={styles.body}>Thank you for participating in the study. As previously stated in the End User License Agreement this app’s purpose is to observe how hesitant a user is about giving information about themselves. The data collected was information the user gave upon registration and checkout. This data includes: name, email, date of birth, shipping information and permissions. The data is recorded into our database and will be used for no other reason than research purposes. </Text>
                        <Text style={styles.body}>In regards to purchases, no real purchases have been made. The app is just imitating a e-commerce app. No charges will be made to the billing information that was provided by the user. In fact, all billing information is not saved what so ever.</Text>
                        <Text style={styles.messageFooter}>Thanks,</Text>
                        <Text style={styles.messageSinc}>- Optimize Prime</Text>
                    </View>
                    <View style={styles.buttonView}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={styles.buttonContainer}>
                        <Text style={styles.buttonText}>Continue</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>

        );
    }
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 100,
        height: 100,
        marginLeft: 140,
        resizeMode: "contain",
        marginTop: 10
    },
    message: {
        paddingHorizontal: 20
    },

     buttonContainer: {
        paddingVertical: 12,
        marginBottom: 20,
        marginTop: 15,
        borderRadius: 7,
        borderWidth: 0,
        backgroundColor: '#800000'
    },
    buttonText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 20
    },
    buttonView: {
        paddingHorizontal: 30
    },
    title: {
        color: '#800000',
        textAlign: 'center',
        marginTop: 10,
        fontSize: 25,
        fontWeight: '400',
        marginBottom: 15,
        paddingHorizontal: 15
    },

    body: {
        marginTop: 5,
        paddingHorizontal: 15,
        color: '#800000',
    },

    messageHeader: {
        color: '#800000',
        fontSize: 18,
        marginBottom: 10

    },

    messageFooter: {
        color: '#800000',
        fontSize: 18,
        marginBottom: 5,
        marginLeft: 200,
        marginTop: 20
    },

    messageSinc: {
        color: '#800000',
        fontSize: 18,
        marginBottom: 5,
        textAlign: 'right'
    }

});

