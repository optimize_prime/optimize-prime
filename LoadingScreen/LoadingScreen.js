/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import firebase from "firebase";

/* Author: Dante DeSantis
 * This class contains functions that communicate with the Database.
**/

export default class LoadingScreen extends React.Component {

    // When the page loades firebase is used to determine if a user is logged in. If not it
    // will send the user to the login screen

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            this.props.navigation.navigate(user ? 'App' : 'Auth');
        });
    } 
    render() {
        return (
            <View style={styles.container}>
                <Text>Loading...</Text>
                <ActivityIndicator size='large'></ActivityIndicator>
            </View>
            
        );
    }
} 
const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

     scrollView: {
        height: "80%",
        marginTop: -15,
        marginBottom: 10,
    },

});

