/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/
import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity,} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import Fire from '.././Firebase/Fire';

/* Author: Dante DeSantis
 * This class will appear after the User enters in data to Register. This class is used to gather the
 * user's permissions and profile picture.
 * This class contains functions that communicate with the Database.
**/

export default class ConfirmationScreen extends React.Component {

    state = {
        user: {},
        image: null

    };

    //When component mounts user will grab data from firebase UserProfile table
    async componentDidMount() {
        const user = this.props.uid || Fire.shared.uid;

        this.unsubscribe = Fire.shared.firestore.collection('UserProfile').doc(user).onSnapshot(doc => {
            this.setState({ user: doc.data() });
        });

    }


    // Get the Photo Permission from the User
    async getPhotoPermission() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        if (status != "granted") {
            alert("We need permission to use your camera roll if you'd like to incude a photo.");
        } else {
            await Fire.shared.cameraPermissions();
        }


    };

    // Get the Location Permission from the User
    async getLocationPermission() {
        const { status } = await Permissions.askAsync(Permissions.LOCATION);

        if (status != "granted") {
            alert("We need permission to use your location");
        } else {
            await Fire.shared.locationPermissions();
        }

    };

    // Get the Contact Permission from the User
    async getContactPermission() {
        const { status } = await Permissions.askAsync(Permissions.CONTACTS);

        if (status != "granted") {
            alert("We need permission to use your Contacts");
        } else {
           await Fire.shared.contactPermissions();
        }

    };

    // pickImage will allow the user to pick an image from camera roll and add it to their profile.
    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3]
        });

        if (!result.cancelled) {
            this.setState({ image: result.uri });
            Fire.shared.addPost({ localUri: this.state.image });
        }
    };


    // When the page loades firebase is used to determine if a user is logged in. If not it
    // will send the user to the login screen
 
    render() {
        this.getPhotoPermission();
        this.getContactPermission();
        this.getLocationPermission();

        return (
            <View style={styles.container}>
                    <View style={styles.scrollPageWrap}>
                        <ScrollView style={styles.scrollPage}>
                            <View style={styles.infoDisplay}>
                                <TouchableOpacity style={styles.avatarContainer} onPress={this.pickImage}>
                                    <Image style={styles.avatar}
                                        source={
                                            this.state.user.image
                                                ? { uri: this.state.user.image }
                                                : require('.././assets/tempAvatar.jpg')
                                        }
                                    />
                                </TouchableOpacity>
                                <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 20, marginBottom: 50 }}>{this.state.user.fName} {this.state.user.lName}</Text>
                                <Text style={styles.emailLabel}>Email:</Text>
                                <Text style={styles.nameInfo}>{this.state.user.email}</Text>
                                <Image
                                    style={styles.line}
                                    source={require('./resources/line.png')}
                                />
                                <Text style={styles.wineLabel}>Favorite Wine:</Text>
                                <Text style={styles.wineInfo}>{this.state.user.alcoholPreference}</Text>
                                <Image
                                    style={styles.line}
                                    source={require('./resources/line.png')}
                                />
                                <Text style={styles.creditLabel}>Credit:</Text>
                                <Text style={styles.creditInfo}>{this.state.user.credit}</Text>
                                <Image
                                    style={styles.line}
                                    source={require('./resources/line.png')}
                            />
                            </View>
                                <TouchableOpacity style={styles.buttonContainer} onPress={() => this.props.navigation.navigate('EULA')} >
                                        <Text style={styles.buttonText}>Confirm</Text>
                                </TouchableOpacity>
                        </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    scrollPageWrap: {
        flex: 7,
        flexDirection: 'column',
    },
    scrollPage: {
        flex: 1,
        flexDirection: 'column',
    },

    infoDisplay: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    avatar: {
        width: 150,
        height: 150,
        borderRadius: 50,
        backgroundColor: '#E1E2E6',
        marginTop: 120,
        justifyContent: 'center',
        alignItems: 'center',
    },

    nameInfo: {
        flex: 1,
        marginTop: -17,
        justifyContent: 'center'
    },

    emailLabel: {
        flex: 1,
        marginLeft: -235,
        marginTop: 20,
        fontWeight: 'bold'
    },

    wineLabel: {
        flex: 1,
        marginLeft: -178,
        marginTop: 20,
        fontWeight: 'bold'
    },

    wineInfo: {
        flex: 1,
        marginTop: -17,
        justifyContent: 'center'
    },

    creditLabel: {
        flex: 1,
        marginLeft: -230,
        marginTop: 20,
        fontWeight: 'bold'
    },

    creditInfo: {
        flex: 1,
        marginTop: -17,
        justifyContent: 'center'
    },

    logout: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#800000',
    },

    buttonContainer: {
        paddingVertical: 12,
        marginBottom: 20,
        marginTop: 15,
        borderRadius: 7,
        borderWidth: 0,
        backgroundColor: '#800000',
        marginTop: 100
    },

    buttonText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#fff',
        fontSize: 20
    },
    
    buttonView: {
        paddingHorizontal: 30
    },



});

