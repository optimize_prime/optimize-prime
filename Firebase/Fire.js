/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/

//import FirebaseKeys from '.././config';
import firebase from "firebase";
import FB from "../screens/firebase.js";
//require("firebase/firestore");

/* Author: Dante DeSantis
 * This class contains functions that communicate with the Database.
**/

class Fire {
    constructor() {

    }

    // addUser => Will connect to firebase and creates a new document for a specefic user with their associated information.
    // db.set will set all the values that were previously defined in UserRegistration.js to firebase.
    // Add a user to the firebase UserProfile table and set all fields.

    addUser = async user => {
        try {
            await firebase.auth().createUserWithEmailAndPassword(user.email, user.password);

            // Creating the document for given user
            let db = this.firestore.collection('UserProfile').doc(this.uid);

            db.set({
                fName: user.fName,
                lName: user.lName,
                email: user.email,
                location: user.zipCode,
                birthDate: user.date,
                alcoholPreference: user.favWine,
                accept: user.accept,
                decline: user.decline,
                credit: user.credit,
                uid: this.uid,
                image: null


            });
        } catch (error) {
            alert('Error: ', error);
        }
    }

    // checkout => will access the ShippingInfo collection and add a new document associated with the User's id.
    // This document will store all shipping information that the user provided.
    addProduct = async => {
        let addDoc = this.firestore.collection('Cart').doc(this.uid);

        addDoc.add({
            UserID: { userID },
            ProductID: { prodID },
            Timestamp: { timestamp },
        });

    }

    // Set the UserProfile picture in the UserProfile table

    addPost = async ({ localUri }) => {
        const remoteUri = await this.uploadPhotoAsync(localUri, `photos/${this.uid}/${Date.now()}`);

        return new Promise((res, rej) => {
            this.firestore
                .collection("UserProfile").doc(this.uid)
                .update({
                    image: remoteUri
                })
                .then(ref => {
                    res(ref);
                })
                .catch(error => {
                    rej(error);
                });
        });
    };

    // Upload the photo to firebase storage for easy access

    uploadPhotoAsync = (uri, filename) => {
        return new Promise(async (res, rej) => {
            const response = await fetch(uri);
            const file = await response.blob();

            let upload = firebase
                .storage()
                .ref(filename)
                .put(file);

            upload.on(
                "state_changed",
                snapshot => { },
                err => {
                    rej(err);
                },
                async () => {
                    const url = await upload.snapshot.ref.getDownloadURL();
                    res(url);
                }
            );
        });
    };


    // Store all checkout information to the firebase ShippingInfo table and set all fields

    checkout = async ship => {
        try {
            let db = this.firestore.collection('ShippingInfo').doc(this.uid);

            db.set({
                streetAddress: ship.streetAdd,
                city: ship.city,
                zip: ship.zip,
                state: ship.state,
            });
        } catch (error) {
            alert('Error: ', error);
        }
    }

    // updateAgreement => will access the UserProfile collection and update the users document associated with them
    // in order to update whether they accepted or decline the EULA.

    // Updates the UserProfile table with a new fields when the User accepts the EULA.

    updateAgreement = async user => {

        try {
            let db = this.firestore.collection('UserProfile').doc(this.uid);

            db.update({
                accept: 1,
                decline: 0
            });
        } catch (error) {
            alert('Error: ', error);
        }
    }

    // onScroll => will access the EULA collection that is associated with that user based on their id.
    // Once accessed a new document will be added to the collection that provides information about how the
    // user interacted with the EULA.
    // Sets fields for the firebase EULA table.

    onScroll = async () => {
        try {
            let db = this.firestore.collection('EULA').doc(this.uid);

            db.set({
                scrolled: 1,
                timer: eula.count
            });
        } catch (error) {
            alert('Error: ', errorMessage);
        }
    }

    // get firestore => returns a way to access the firebase in order to read/write to it.
    // Sets fields for the firebase UserProfile table

    cameraPermissions = async user => {
        try {
            let db = this.firestore.collection('UserProfile').doc(this.uid);
            db.update({
                cameraPermissions: 1
            });
        } catch (error) {
            alert('Error: ', error);
        }
    }

    // Sets fields for the firebase UserProfile table

    locationPermissions = async user => {
        try {
            let db = this.firestore.collection('UserProfile').doc(this.uid);
            db.update({
                locationPermissions: 1
            });
        } catch (error) {
            alert('Error: ', error);
        }
    }

    // Sets fields for the firebase UserProfile table

    contactPermissions = async user => {
        try {
            let db = this.firestore.collection('UserProfile').doc(this.uid);
            db.update({
                contactsPermissions: 1
            });
        } catch (error) {
            alert('Error: ', error);
        }
    }

    get firestore() {
        return firebase.firestore();
    }

    // get uid => will access firebase database and get the current user's id.
    // Returns current user ID
    get uid() {
        return (firebase.auth().currentUser || {}).uid;
    }


    get timestamp() {
        return Data.now();
    }
}

Fire.shared = new Fire();
export default Fire;
