/*
This file is part of Dr. Zheng's Research Application.

Dr. Zheng's Research Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dr. Zheng's Research Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dr. Zheng's Research Application.  
If not, see <https://www.gnu.org/licenses/>.
*/


import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import FeatProduct from './screens/featuredProducts.js';
import ProductScreen from './screens/ProductScreen.js';
import SearchScreen from './screens/searchScreen.js';
import UserRegistration from './Account/UserRegistration/UserRegistration.js';
import EULA from './Account/EULA/EULA.js';
import Login from './Account/Login/Login.js';
import LoadingScreen from './LoadingScreen/LoadingScreen.js';
import CartScreen from './screens/CartScreen.js';
import AccountSettings from './Account/Settings/AccountSettings.js';
import CheckoutScreen from './screens/CheckoutScreen.js';
import ThankyouScreen from './LoadingScreen/ThankyouScreen.js';
import ConfirmationScreen from './LoadingScreen/ConfirmationScreen.js';

// The main function that is getting rendered to the screen
export default class App extends React.Component {
  
  render() {
    return <AppContainer />;
  }
}

const AuthStack = createStackNavigator({
    Login: {
    	screen: Login,
    	navigationOptions: {
          header: null //this will hide the header
      }
    },
    UserRegistration: {
    	screen: UserRegistration,
    	navigationOptions: {
          header: null //this will hide the header
      }
    },
    ConfirmationScreen: {
        screen: ConfirmationScreen,
        navigationOptions: {
            header: null //this will hide the header
        }
    },
    EULA: {
    	screen: EULA,
    	navigationOptions: {
          header: null //this will hide the header
      }
    },

});

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: FeatProduct,
      navigationOptions: {
          header: null //this will hide the header
      }
    },
    ProductScreen: {
    	screen: ProductScreen,
    	navigationOptions: {
            header: null //this will hide the header
    	}
    },
    SearchScreen: {
    	screen: SearchScreen,
    	navigationOptions: {
            header: null //this will hide the header
    	}
    },
    CartScreen: {
    	screen: CartScreen,
    	navigationOptions: {
            header: null //this will hide the header
    	}
    },
    Settings: {
        screen: AccountSettings,
        navigationOptions: {
            header: null //this will hide the header
        }
    },
    CheckoutScreen: {
        screen: CheckoutScreen,
        navigationOptions: {
            header: null //this will hide the header
        }
    },
    ThankyouScreen: {
        screen: ThankyouScreen,
        navigationOptions: {
            header: null //this will hide the header
        }
    },
}
);


const AppContainer = createAppContainer( 
	createSwitchNavigator(
        {
            Loading: LoadingScreen,
            App: AppNavigator,
            Auth: AuthStack,
        },
        {
            initialRouteName: "Loading"
        }
    )
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
